package com.netcracker;

import com.netcracker.Ball.Ball;
import com.netcracker.Ball.Container;

public class Main {
    public static void main(String[] args) {
        Ball ball = new Ball(20,10,2,15,20);
        Container container = new Container(-10,10,5,5);
        System.out.println(container.toString());
        System.out.println("Ball start point:\n"+ball.toString());
        System.out.print("Is in container? ");
        System.out.println(container.collides(ball));
        ball.move();
        System.out.println("The ball has moved. Current location: ");
        System.out.println(ball.toString());
        ball.move();
        System.out.println("The ball has moved. Current location: ");
        System.out.println(ball.toString());
        System.out.print("Is in container? ");
        System.out.println(container.collides(ball));
        ball.reflectHorisontal();
        System.out.println("The ball bounced off a vertical wall");
        ball.move();
        ball.move();
        ball.move();
        ball.move();
        ball.move();
        System.out.println("The ball has moved. Current location: ");
        System.out.println(ball.toString());
        System.out.print("Is in container? ");
        System.out.println(container.collides(ball));
    }
}
